using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AzureFunctionTest
{
    public static class Function1
    {
        [FunctionName("checkGetMethod")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string name = req.Query["name"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            name = name ?? data?.name;

            string responseMessage = string.IsNullOrEmpty(name)
                ? "This HTTP triggered function executed successfully."
                : $"Hello, welcome {name} !!!";

            return new OkObjectResult(responseMessage);
        }

        [FunctionName("checkPostMethod")]
        public static async Task<IActionResult> RunSecondFnction(
            [HttpTrigger(AuthorizationLevel.Function,   "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");
            //string jsonContent = await req.Body.ReadAsStringAsync();
            string name = req.Query["name"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            CommonRequestBody data = JsonConvert.DeserializeObject<CommonRequestBody>(requestBody);
            //dynamic data = JsonConvert.DeserializeObject(requestBody);
            //name = name ?? data?.userName;

            string responseMessage = string.IsNullOrEmpty(name)
                ? "This HTTP triggered function executed successfully."
                : $"Hello, you have logged in with {name} !!!";
            if (data != null)
            {
                return new OkObjectResult(data);
            }
            else
            {
                return new OkObjectResult(responseMessage);
            }
            
        }
    }
}
