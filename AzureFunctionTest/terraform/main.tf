terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      # Root module should specify the maximum provider version
      # The ~> operator is a convenient shorthand for allowing only patch releases within a specific minor release.
      version = "~> 2.26"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "resource_group" {
  name = "${var.project}-${var.environment}-resource-group"
  location = var.location
}

resource "azurerm_storage_account" "storage_account" {
  name = "${var.project}${var.environment}"
  resource_group_name = azurerm_resource_group.resource_group.name
  location = var.location
  account_tier = "Standard"
  account_replication_type = "LRS"


}

resource "azurerm_storage_container" "storage_container" {
  name                  = "${var.project}-${var.environment}-storage-container"
  storage_account_name  = azurerm_storage_account.storage_account.name
  container_access_type = "private"
}

data "archive_file" "zipfolder" {
  type        = "zip"
  source_dir  = var.functionapp_source
  output_path = var.functionapp
}

resource "azurerm_storage_blob" "appcode" {
  name                   = var.storage_blob
  storage_account_name   = azurerm_storage_account.storage_account.name
  storage_container_name = azurerm_storage_container.storage_container.name
  type                   = "Block"
  source                 = var.functionapp
}



data "azurerm_storage_account_blob_container_sas" "storage_account_blob_container_sas" {
  connection_string = azurerm_storage_account.storage_account.primary_connection_string
  container_name    = azurerm_storage_container.storage_container.name

  start  = "2021-01-01T00:00:00Z"
  expiry = "2025-01-01T00:00:00Z"

  permissions {
    read   = true
    add    = false
    create = false
    write  = false
    delete = false
    list   = false
  }
}

resource "azurerm_application_insights" "application_insights" {
  name                = "${var.project}-${var.environment}-application-insights"
  location            = var.location
  resource_group_name = azurerm_resource_group.resource_group.name
  application_type    = "web"
}

resource "azurerm_app_service_plan" "app_service_plan" {
  name                = "${var.project}-${var.environment}-app-service-plan"
  resource_group_name = azurerm_resource_group.resource_group.name
  location            = var.location
  kind     = "FunctionApp"
  reserved = false
  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

resource "azurerm_function_app" "function_app" {
  name                       = "${var.project}-${var.environment}-function-app"
  resource_group_name        = azurerm_resource_group.resource_group.name
  location                   = var.location
  app_service_plan_id        = azurerm_app_service_plan.app_service_plan.id
  os_type 		     = "linux"
  storage_account_name       = azurerm_storage_account.storage_account.name
  storage_account_access_key = azurerm_storage_account.storage_account.primary_access_key
  version                    = "~3"

  app_settings = {
    "WEBSITE_RUN_FROM_PACKAGE"    = "https://${azurerm_storage_account.storage_account.name}.blob.core.windows.net/${azurerm_storage_container.storage_container.name}/${azurerm_storage_blob.appcode.name}${data.azurerm_storage_account_blob_container_sas.storage_account_blob_container_sas.sas}",
    "FUNCTIONS_WORKER_RUNTIME"    = "dotnet",
    "AzureWebJobsDisableHomepage" = "true",
  }

}

# data "azurerm_function_app_host_keys" "host_keys" {
#  name                = azurerm_function_app.function_app.name
#  resource_group_name = azurerm_function_app.function_app.resource_group_name

#  depends_on = [azurerm_function_app.function_app]
#}

#locals {
#    function_name="{document_group}/{document_type}/{document_unique_id}"
#    function_name1="query/{document_group}"
#}
#resource "null_resource" "func_publish" {
#provisioner "local-exec" {
#command = "../code.bat \"https://${azurerm_function_app.function_app.default_hostname}/${local.function_name}\" \"https://${azurerm_function_app.function_app.default_hostname}/${local.function_name}\" \"https://${azurerm_function_app.function_app.default_hostname}/${local.function_name}\" \"https://${azurerm_function_app.function_app.default_hostname}/${local.function_name1}\""
#interpreter = ["PowerShell", "-Command"]
#}
#depends_on = [azurerm_function_app.function_app]    
#}

output "function_app_name" {
  value = azurerm_function_app.function_app.name
  description = "Deployed function app name"
}

output "function_app_default_hostname" {
  value = azurerm_function_app.function_app.default_hostname
  description = "Deployed function app hostname"
}

#output "url" {

#  value = "https://${azurerm_function_app.function_app.default_hostname}/api/${local.function_name}?code=${data.azurerm_function_app_host_keys.host_keys.default_function_key}"
#  sensitive = true
#}