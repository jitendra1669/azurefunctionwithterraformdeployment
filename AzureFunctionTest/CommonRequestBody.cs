﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AzureFunctionTest
{
    public class CommonRequestBody
    {
        public string userName { get; set; }
        public string password { get; set; }
        public bool IsCheck { get; set; }
        public Employees Employees { get; set; }
    }

    public class Employees
    {
        public string MethodName { get; set; }
        public object[] entities { get; set; }
        public bool IsActive { get; set; }
        public string text { get; set; }
    }
}
